<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace ZF\Rest;

use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use ZF\ApiProblem\ApiProblem;

/**
 * Class AbstractResourceListener
 * @method create($data)
 * @method delete($id)
 * @method deleteList($data)
 * @method fetch($id)
 * @method fetchAll($queryParams)
 * @method patch($id, $data)
 * @method patchList($data)
 * @method replaceList($data)
 * @method update($id, $data)
 * @package ZF\Rest
 */
abstract class AbstractResourceListener extends AbstractListenerAggregate
{
    /**
     * @var ResourceEvent
     */
    protected $event;

    /**
     * The entity_class config for the calling controller zf-rest config
     */
    protected $entityClass;

    /**
     * The collection_class config for the calling controller zf-rest config
     */
    protected $collectionClass;

    /**
     * Current identity, if discovered in the resource event.
     *
     * @var \ZF\MvcAuth\Identity\IdentityInterface
     */
    protected $identity;

    /**
     * Input filter, if discovered in the resource event.
     *
     * @var \Zend\InputFilter\InputFilterInterface
     */
    protected $inputFilter;

    /**
     * Set the entity_class for the controller config calling this resource
     */
    public function setEntityClass($className)
    {
        $this->entityClass = $className;
        return $this;
    }

    public function getEntityClass()
    {
        return $this->entityClass;
    }

    public function setCollectionClass($className)
    {
        $this->collectionClass = $className;
        return $this;
    }

    public function getCollectionClass()
    {
        return $this->collectionClass;
    }

    /**
     * Retrieve the current resource event, if any
     *
     * @return ResourceEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Retrieve the identity, if any
     *
     * Proxies to the resource event to find the identity, if not already
     * composed, and composes it.
     *
     * @return null|\ZF\MvcAuth\Identity\IdentityInterface
     */
    public function getIdentity()
    {
        if ($this->identity) {
            return $this->identity;
        }

        $event = $this->getEvent();
        if (! $event instanceof ResourceEvent) {
            return null;
        }

        $this->identity = $event->getIdentity();
        return $this->identity;
    }

    /**
     * Retrieve the input filter, if any
     *
     * Proxies to the resource event to find the input filter, if not already
     * composed, and composes it.
     *
     * @return null|\Zend\InputFilter\InputFilterInterface
     */
    public function getInputFilter()
    {
        if ($this->inputFilter) {
            return $this->inputFilter;
        }

        $event = $this->getEvent();
        if (! $event instanceof ResourceEvent) {
            return null;
        }

        $this->inputFilter = $event->getInputFilter();
        return $this->inputFilter;
    }

    /**
     * Attach listeners for all Resource events
     *
     * @param  EventManagerInterface $events
     */
    public function attach(EventManagerInterface $events)
    {
        $events->attach('create', [$this, 'dispatch']);
        $events->attach('delete', [$this, 'dispatch']);
        $events->attach('deleteList', [$this, 'dispatch']);
        $events->attach('fetch', [$this, 'dispatch']);
        $events->attach('fetchAll', [$this, 'dispatch']);
        $events->attach('patch', [$this, 'dispatch']);
        $events->attach('patchList', [$this, 'dispatch']);
        $events->attach('replaceList', [$this, 'dispatch']);
        $events->attach('update', [$this, 'dispatch']);
    }

    /**
     * Dispatch an incoming event to the appropriate method
     *
     * Marshals arguments from the event parameters.
     *
     * @param  ResourceEvent $event
     * @return mixed
     */
    public function dispatch(ResourceEvent $event)
    {
        $this->event = $event;
        switch ($event->getName()) {
            case 'create':
                $data = $event->getParam('data', []);
                return $this->create($data);
            case 'delete':
                $id   = $event->getParam('id', null);
                return $this->delete($id);
            case 'deleteList':
                $data = $event->getParam('data', []);
                return $this->deleteList($data);
            case 'fetch':
                $id   = $event->getParam('id', null);
                return $this->fetch($id);
            case 'fetchAll':
                $queryParams = $event->getQueryParams() ?: [];
                return $this->fetchAll($queryParams);
            case 'patch':
                $id   = $event->getParam('id', null);
                $data = $event->getParam('data', []);
                return $this->patch($id, $data);
            case 'patchList':
                $data = $event->getParam('data', []);
                return $this->patchList($data);
            case 'replaceList':
                $data = $event->getParam('data', []);
                return $this->replaceList($data);
            case 'update':
                $id   = $event->getParam('id', null);
                $data = $event->getParam('data', []);
                return $this->update($id, $data);
            default:
                throw new Exception\RuntimeException(sprintf(
                    '%s has not been setup to handle the event "%s"',
                    __METHOD__,
                    $event->getName()
                ));
        }
    }

    /**
     * Permit to avoid the following scrutinizer error:
     *      "The return type of $candidateEntity; (CandidateApi\V1\Rest\Pub...e\CandidateEntity)
     *      is incompatible with the return type of the parent
     *      method ZF\Rest\AbstractResourceListener::create of type ZF\ApiProblem\ApiProblem."
     *
     * @param $name
     * @param $arguments
     * @return ApiProblem
     */
    public function __call($name, $arguments)
    {
        $methodNameErrorMessageMap = [
            'create'        => 'The POST method has not been defined',
            'delete'        => 'The DELETE method has not been defined for individual resources',
            'deleteList'    => 'The DELETE method has not been defined for collections',
            'fetch'         => 'The GET method has not been defined for individual resources',
            'fetchAll'      => 'The GET method has not been defined for collections',
            'patch'         => 'The PATCH method has not been defined for individual resources',
            'patchList'     => 'The PATCH method has not been defined for collections',
            'replaceList'   => 'The PUT method has not been defined for collections',
            'update'        => 'The PUT method has not been defined for individual resources'
        ];

        if(in_array($methodNameErrorMessageMap, $name)) {
            return new ApiProblem(405, $methodNameErrorMessageMap['name']);
        }
    }
}
